import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueLineRecordReader;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.math.RoundingMode.HALF_UP;

public class candle {
    public static class CandleCompositeKey implements WritableComparable<CandleCompositeKey> {
        public String symbol;
        public String moment;
        public String id_deal;

        public CandleCompositeKey() {
        }

        public CandleCompositeKey(String symbol, String moment, String id_deal) {
            super();
            this.set(symbol, moment, id_deal);
        }

        public void set(String symbol, String moment, String id_deal) {
            this.symbol = (symbol == null) ? "" : symbol;
            this.moment = (moment == null) ? "" : moment;
            this.id_deal = (id_deal == null) ? "" : id_deal;
        }

        @Override
        public void write(DataOutput out) throws IOException {
            out.writeUTF(symbol);
            out.writeUTF(moment);
            out.writeUTF(id_deal);
        }

        @Override
        public void readFields(DataInput in) throws IOException {
            symbol = in.readUTF();
            moment = in.readUTF();
            id_deal = in.readUTF();
        }

        @Override
        public int compareTo(CandleCompositeKey o) {
            int symCmp = symbol.compareTo(o.symbol);
            if (symCmp != 0) {
                return symCmp;
            } else {
                int momCmp = moment.compareTo(o.moment);
                if (momCmp != 0) {
                    return momCmp;
                } else {
                    return id_deal.compareTo(o.id_deal);
                }
            }
        }
    }

    public static class HeaderInputFormat extends FileInputFormat<Text, Text> {
        Text header;
        String[] record = new String[4];
        Map<Integer, String> pos2field = new HashMap<>();

        @Override
        protected boolean isSplitable(JobContext context, Path file) {
            return false;
        }

        @Override
        public RecordReader<Text, Text> createRecordReader(InputSplit arg0, TaskAttemptContext arg1) throws IOException {
            return new KeyValueLineRecordReader(arg1.getConfiguration()) {

                @Override
                public void initialize(InputSplit genericSplit, TaskAttemptContext context) throws IOException {
                    super.initialize(genericSplit, context);
                    super.nextKeyValue();
                    header = super.getCurrentKey();
                    if (header.toString().length() == 0) {
                        throw new RuntimeException("No header :(");
                    }
                    String[] words = header.toString().split(",");
                    int pos = 0;
                    for (String s: words) {
                        pos2field.put(pos, s);
                        pos++;
                    }
                }

                @Override
                public Text getCurrentKey() {
                    return this.getCurrentValue();
                }

                @Override
                public Text getCurrentValue() {
                    String raw = super.getCurrentKey().toString();
                    String[] fields = raw.split(",");
                    int pos = 0;
                    // #SYMBOL, MOMENT, ID_DEAL, PRICE_DEAL
                    for (String f: fields) {
                        String field = pos2field.get(pos);
                        switch (field) {
                            case "#SYMBOL":
                                record[0] = f;
                                break;
                            case "MOMENT":
                                record[1] = f;
                                break;
                            case "ID_DEAL":
                                record[2] = f;
                                break;
                            case "PRICE_DEAL":
                                record[3] = f;
                                break;
                        }
                        pos++;
                    }
                    String filteredLine = record[0] + "," + record[1] + "," + record[2] + "," + record[3];
                    return new Text(filteredLine);
                }
            };
        }
    }


    public static class CandleMapper extends Mapper<Object, Text, CandleCompositeKey, FloatWritable> {
//        int i = 0;
        private CandleCompositeKey curKey = new CandleCompositeKey();
        private FloatWritable curVal = new FloatWritable();
        private long dateFrom, dateTo, timeFrom, timeTo;
        private String secRegex;

        protected void setup(Context context) {
            Configuration conf = context.getConfiguration();
            secRegex = conf.get("candle.securities");
            dateFrom = Long.parseLong(conf.get("candle.date.from"));
            dateTo = Long.parseLong(conf.get("candle.date.to"));
            timeFrom = Long.parseLong(conf.get("candle.time.from"));
            timeTo = Long.parseLong(conf.get("candle.time.to"));
        }

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            // #SYMBOL, MOMENT, ID_DEAL, PRICE_DEAL

            String[] fields = value.toString().split(",");
            long date = Long.parseLong(fields[1].substring(0, 8));
            long time = Long.parseLong(fields[1].substring(8, 12));
            boolean notActSymbol = !(fields[0].matches(secRegex));
            boolean notActDate = date < dateFrom || date >= dateTo;
            boolean notActTime = time < timeFrom || time >= timeTo;

            if (notActSymbol || notActDate || notActTime) {
                return;
            }
            curKey.set(fields[0], fields[1], fields[2]);
            curVal.set(Float.parseFloat(fields[3]));
            context.write(curKey, curVal);
        }
    }


    public static class CandlePartitioner extends Partitioner<CandleCompositeKey, FloatWritable> {
        @Override
        public int getPartition(CandleCompositeKey key, FloatWritable value, int numReduceTasks) {
            String mainKey = key.symbol;
            return mainKey.hashCode() % numReduceTasks;
        }
    }

    public static class CandleGroupComparator extends WritableComparator {
        public CandleGroupComparator() {
            super(CandleCompositeKey.class, true);
        }

        @Override
        public int compare(WritableComparable wc1, WritableComparable wc2) {
            CandleCompositeKey k1 = (CandleCompositeKey) wc1;
            CandleCompositeKey k2 = (CandleCompositeKey) wc2;
            return k1.symbol.compareTo(k2.symbol);
        }
    }


    public static class CandleReducer extends Reducer<CandleCompositeKey, FloatWritable, NullWritable, Text> {
        private MultipleOutputs mos;
        private String timeFrom, timeTo;
        private long width;

        public void setup(Context context) {
            mos = new MultipleOutputs(context);
            Configuration conf = context.getConfiguration();
            timeFrom = conf.get("candle.time.from");
            timeTo = conf.get("candle.time.to");
            width = Long.parseLong(conf.get("candle.width"));
        }

        public void cleanup(Context context) throws IOException, InterruptedException {
            mos.close();
        }

        public String update(String time) {
            Calendar calendarTime = new GregorianCalendar();
            calendarTime.clear();
            Calendar calendarTimeFrom = new GregorianCalendar();
            calendarTimeFrom.clear();
            Calendar calendarTimeTo = new GregorianCalendar();
            calendarTimeTo.clear();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.ENGLISH);
            try {
                calendarTime.setTime(sdf.parse(time));
                calendarTimeFrom.setTime(sdf.parse(time.substring(0,8) + timeFrom + "00" + "000"));
                calendarTimeTo.setTime(sdf.parse(time.substring(0,8) + timeTo + "00" + "000"));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long sum = calendarTime.getTimeInMillis() + width;
            calendarTime.setTimeInMillis(sum);
            String strTime;
            // if time + width < timeto return time + width
            if (calendarTime.compareTo(calendarTimeTo) < 0) {
                strTime = sdf.format(calendarTime.getTime());
            } else {
                // else return timeFrom + width of next day
                sum = calendarTimeFrom.getTimeInMillis() + width + 24 * 60 * 60 * 1000;
                calendarTimeFrom.setTimeInMillis(sum);
                strTime = sdf.format(calendarTimeFrom.getTime());
            }

            return strTime;
        }

        public void reduce(CandleCompositeKey key, Iterable<FloatWritable> values, Context context) throws IOException, InterruptedException {
            String curCandleMoment = key.moment.substring(0, 8) + timeFrom + "00" + "000";
            String prevCandleMoment = curCandleMoment;
            String symbol = key.symbol;
            String time;
            BigDecimal min, max, prev, open;
            open = max = prev = new BigDecimal(Float.toString(0));
            min = new BigDecimal(Float.toString(Float.MAX_VALUE));

            for (FloatWritable val: values) {
                time = key.moment;

                // day of time < day of candle (occur on last uncomplete candle)
                if (time.substring(6, 8).compareTo(prevCandleMoment.substring(6, 8)) < 0) {
                    open = max = prev = new BigDecimal(Float.toString(0));
                    min = new BigDecimal(Float.toString(Float.MAX_VALUE));
                    continue;
                }

                if (time.compareTo(curCandleMoment) < 0) {
                    prev = new BigDecimal(Float.toString(val.get()));
                    if (min.compareTo(prev) > 0) {
                        min = prev;
                    }
                    if (max.compareTo(prev) < 0) {
                        max = prev;
                    }
                    if (open.compareTo(new BigDecimal(Float.toString(0))) == 0) {
                        open = prev;
                    }
                } else {
                    // open == 0 before first timeTo candle
                    if (open.compareTo(new BigDecimal(Float.toString(0))) != 0) {
                        mos.write(NullWritable.get(), new Text(
                                symbol + "," +
                                prevCandleMoment + "," +
                                open.setScale(1, HALF_UP).toString() + "," +
                                max.setScale(1, HALF_UP).toString() + "," +
                                min.setScale(1, HALF_UP).toString() + "," +
                                prev.setScale(1, HALF_UP).toString()), symbol);
                    }
                    prevCandleMoment = curCandleMoment;
                    curCandleMoment = update(curCandleMoment);
                    while (time.compareTo(curCandleMoment) >= 0) {
                        prevCandleMoment = curCandleMoment;
                        curCandleMoment = update(curCandleMoment);
                    }
                    open = new BigDecimal(Float.toString(val.get()));
                    min = max = prev = open;
                }
            }
            if (open.compareTo(new BigDecimal(Float.toString(0))) != 0) {
                mos.write(NullWritable.get(), new Text(
                        symbol + "," +
                        prevCandleMoment + "," +
                        open.setScale(1, HALF_UP).toString() + "," +
                        max.setScale(1, HALF_UP).toString() + "," +
                        min.setScale(1, HALF_UP).toString() + "," +
                        prev.setScale(1, HALF_UP).toString()), symbol);
            }
        }
    }

    private static void setConf(Configuration conf) {
        if (conf.get("candle.width") == null) {
            conf.set("candle.width", "300000");
        }
        if (conf.get("candle.securities") == null) {
            conf.set("candle.securities", ".*");
        }
        if (conf.get("candle.date.from") == null) {
            conf.set("candle.date.from", "19000101");
        }
        if (conf.get("candle.date.to") == null) {
            conf.set("candle.date.to", "20200101");
        }
        if (conf.get("candle.time.from") == null) {
            conf.set("candle.time.from", "1000");
        }
        if (conf.get("candle.time.to") == null) {
            conf.set("candle.time.to", "1800");
        }
        if (conf.get("candle.num.reducers") == null) {
            conf.set("candle.num.reducers", "10");
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        setConf(conf);
        if (otherArgs.length != 2) {
            System.err.println("Usage: candle <in> <out>");
            System.exit(2);
        }

        Job job = new Job(conf, "candle");
        job.setJarByClass(candle.class);
        job.setInputFormatClass(HeaderInputFormat.class);
        job.setMapperClass(CandleMapper.class);
        job.setMapOutputKeyClass(CandleCompositeKey.class);
        job.setMapOutputValueClass(FloatWritable.class);
        job.setPartitionerClass(CandlePartitioner.class);
        job.setGroupingComparatorClass(CandleGroupComparator.class);
        job.setNumReduceTasks(Integer.parseInt(conf.get("candle.num.reducers")));
        job.setReducerClass(CandleReducer.class);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
        FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
        LazyOutputFormat.setOutputFormatClass(job, TextOutputFormat.class);
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}