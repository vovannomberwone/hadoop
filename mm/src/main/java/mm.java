import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;

import static java.math.RoundingMode.HALF_UP;

public class mm {
    public static class MMCompositeKey implements WritableComparable<MMCompositeKey> {
        public Integer g1, g2;

        public MMCompositeKey() {
        }

        public MMCompositeKey(Integer g1, Integer g2) {
            super();
            this.set(g1, g2);
        }

        public void set(Integer g1, Integer g2) {
            this.g1 = g1;
            this.g2 = g2;
        }

        @Override
        public void write(DataOutput out) throws IOException {
            out.writeUTF(String.valueOf(g1));
            out.writeUTF(String.valueOf(g2));
        }

        @Override
        public void readFields(DataInput in) throws IOException {
            g1 = Integer.parseInt(in.readUTF());
            g2 = Integer.parseInt(in.readUTF());
        }

        @Override
        public int compareTo(MMCompositeKey o) {
            int g1Cmp = g1.compareTo(o.g1);
            int g2Cmp = g2.compareTo(o.g2);

            if (g1Cmp != 0) {
                return g1Cmp;
            } else {
                return g2Cmp;
            }
        }
    }


    public static class MMCompositeVal implements Writable {
        public Integer i, j;
        public Float val;
        public String tag;

        public MMCompositeVal() {
        }

        public MMCompositeVal(Integer i, Integer j, Float val, String tag) {
            super();
            this.set(i, j, val, tag);
        }

        public void set(Integer i, Integer j, Float val, String tag) {
            this.i = i;
            this.j = j;
            this.val = val;
            this.tag = (tag == null) ? "" : tag;
        }

        @Override
        public void write(DataOutput out) throws IOException {
            out.writeUTF(String.valueOf(i));
            out.writeUTF(String.valueOf(j));
            out.writeUTF(String.valueOf(val));
            out.writeUTF(tag);
        }

        @Override
        public void readFields(DataInput in) throws IOException {
            i = Integer.parseInt(in.readUTF());
            j = Integer.parseInt(in.readUTF());
            val = Float.parseFloat(in.readUTF());
            tag = in.readUTF();
        }
    }


    public static class MMMapper extends Mapper<Object, Text, MMCompositeKey, MMCompositeVal> {
        private MMCompositeKey curKey = new MMCompositeKey();
        private MMCompositeVal curVal = new MMCompositeVal();
        private String[] tags;
        private Integer groupSize;
        private Integer groups;

        protected void setup(Context context) {
            Configuration conf = context.getConfiguration();
            tags = conf.get("mm.tags").split("(?!^)");
            Integer matrixSize = Integer.parseInt(conf.get("mm.size"));
            groups = Integer.parseInt(conf.get("mm.groups"));
            groupSize = matrixSize / groups;
        }

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String[] fields = value.toString().split("\t");
            String tag = fields[0];
            Integer i = Integer.parseInt(fields[1]);
            Integer j = Integer.parseInt(fields[2]);
            Float val = Float.parseFloat(fields[3]);

            Integer gr1 = i / groupSize;
            Integer gr2 = j / groupSize;

            if (tag.equals(tags[0])) {
                i %= groupSize;
                if (gr1.equals(groups)) {
                    i += groupSize;
                    gr1 = groups - 1;
                }
                curVal.set(i, j, val, tag);
                for (int g2 = 0; g2 < groups; g2++) {
                    curKey.set(gr1, g2);
                    context.write(curKey, curVal);
                }
            } else {
                j %= groupSize;
                if (gr2.equals(groups)) {
                    j += groupSize;
                    gr2 = groups - 1;
                }
                curVal.set(i, j, val, tag);
                for (int g1 = 0; g1 < groups; g1++) {
                    curKey.set(g1, gr2);
                    context.write(curKey, curVal);
                }
            }
        }
    }


    public static class MMReducer extends Reducer<MMCompositeKey, MMCompositeVal, NullWritable, Text> {
        private String[] tags;
        private Integer matrixSize, groupSize, groups;
        private int format;

        public void setup(Context context) {
            Configuration conf = context.getConfiguration();
            tags = conf.get("mm.tags").split("(?!^)");
            matrixSize = Integer.parseInt(conf.get("mm.size"));
            groups = Integer.parseInt(conf.get("mm.groups"));
            groupSize = matrixSize / groups;
            String fmt = conf.get("mm.float-format");
            format = Integer.parseInt(fmt.substring(fmt.length() - 2, fmt.length() - 1));
        }

        public void reduce(MMCompositeKey key, Iterable<MMCompositeVal> values, Context context) throws IOException, InterruptedException {
            Integer n, m;

            if (key.g1 == (groups - 1)) {
                n = groupSize + matrixSize % groupSize;
            } else {
                n = groupSize;
            }
            if (key.g2 == (groups - 1)) {
                m = groupSize + matrixSize % groupSize;
            } else {
                m = groupSize;
            }
            double[][] a = new double[n][matrixSize];
            double[][] b = new double[matrixSize][m];

            for (MMCompositeVal val: values) {
                if (val.tag.equals(tags[0])) {
                    a[val.i][val.j] = val.val;
                } else {
                    b[val.i][val.j] = val.val;
                }
            }

            RealMatrix A = new Array2DRowRealMatrix(a);
            RealMatrix B = new Array2DRowRealMatrix(b);
            RealMatrix C = A.multiply(B);
            double[][] c = C.getData();

            int i_out = key.g1 * groupSize;
            for (int i = 0; i < n; i++) {
                int j_out = key.g2 * groupSize;
                for (int j = 0; j < m; j++) {
                    BigDecimal outVal = BigDecimal.valueOf(c[i][j]).setScale(format, HALF_UP);

                    if (outVal.compareTo(BigDecimal.ZERO) != 0) {
                        context.write(
                                NullWritable.get(),
                                new Text(tags[2] + "\t" + i_out + "\t" + j_out + "\t" +
                                        outVal));
                    }
                    j_out++;
                }
                i_out++;
            }
        }
    }

    private static void setConf(Configuration conf) {
        if (conf.get("mm.groups") == null) {
            conf.set("mm.groups", "1");
        }
        if (conf.get("mm.tags") == null) {
            conf.set("mm.tags", "ABC");
        }
        if (conf.get("mm.float-format") == null) {
            conf.set("mm.float-format", "%.3f");
        }
        if (conf.get("mapred.reduce.tasks") == null) {
            conf.set("mapred.reduce.tasks", "1");
        }
    }

    private static String readFileFromHDFS(String path) throws IOException {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        Path inFile = new Path(path);
        if (!fs.exists(inFile)) {
            throw new IOException("Input file " + path + "not found");
        }
        FSDataInputStream in = fs.open(inFile);
        byte[] buffer = new byte[256];
        in.read(buffer);
        Integer size = Integer.parseInt(new String(buffer, StandardCharsets.UTF_8).split("\t")[0]);
        String s = new String(String.valueOf(size) + "\t" + size);
        in.close();
        fs.close();
        return s;
    }

    private static void writeFileToHDFS(String path, String s) throws IOException {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        Path outFile = new Path(path);
        if (fs.exists(outFile)) {
            fs.delete(outFile,true);
        }
        OutputStream os = fs.create(outFile);
        BufferedWriter br = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));
        br.write(s);
        br.close();
        fs.close();
    }

    private static String getMatrixSize(String[] paths) throws IOException {
        String size = readFileFromHDFS(paths[0] + "/size");
        writeFileToHDFS(paths[2] + "/size", size);
        return size.split("\t")[0];
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        setConf(conf);
        if (otherArgs.length != 3) {
            System.err.println("Usage: mm <path A> <path B> <path C>");
            System.exit(1);
        }
        String size = getMatrixSize(otherArgs);
        conf.set("mm.size", size);

        Job job = new Job(conf, "mm");
        job.setJarByClass(mm.class);
        job.setMapperClass(MMMapper.class);
        job.setMapOutputKeyClass(MMCompositeKey.class);
        job.setMapOutputValueClass(MMCompositeVal.class);
        job.setNumReduceTasks(Integer.parseInt(conf.get("mapred.reduce.tasks")));
        job.setReducerClass(MMReducer.class);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path(otherArgs[0] + "/data"));
        FileInputFormat.addInputPath(job, new Path(otherArgs[1] + "/data"));
        FileOutputFormat.setOutputPath(job, new Path(otherArgs[2] + "/data"));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}